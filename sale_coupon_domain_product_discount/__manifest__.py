# Copyright 2022 Ooops404
# Copyright 2022 Tecnativa - David Vidal
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "Coupon Promotion Product Domain Discount",
    "version": "2.0.1.1.0",
    "summary": "Apply discount only to the domain matching products",
    "author": "Ooops, Cetmix, Tecnativa, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "category": "Sales Management",
    "website": "https://gitlab.com/flectra-community/sale-promotion",
    "depends": ["sale_coupon"],
    "data": ["views/coupon_program_views.xml"],
    "installable": True,
    "application": False,
}
