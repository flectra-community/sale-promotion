# Flectra Community / sale-promotion

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[sale_coupon_reward_fixed_price](sale_coupon_reward_fixed_price/) | 2.0.1.0.0| Apply fixed price for domain matching products
[sale_coupon_reward_add_product](sale_coupon_reward_add_product/) | 2.0.1.0.0| Sale Coupon Automatic free product as normal
[sale_coupon_criteria_multi_product](sale_coupon_criteria_multi_product/) | 2.0.1.0.0| Allows to set as promotion criteria multi-product conditions
[sale_coupon_domain_product_discount](sale_coupon_domain_product_discount/) | 2.0.1.1.0| Apply discount only to the domain matching products
[sale_coupon_order_line_link](sale_coupon_order_line_link/) | 2.0.1.0.1| Adds a link between coupons and their generated order lines for easing tracking
[sale_coupon_criteria_order_based](sale_coupon_criteria_order_based/) | 2.0.1.0.0| Allows to set a sales order related domain for coupon
[sale_coupon_auto_refresh](sale_coupon_auto_refresh/) | 2.0.1.0.3| Allows to auto-apply the coupons with no user intervention
[sale_coupon_delivery_auto_refresh](sale_coupon_delivery_auto_refresh/) | 2.0.1.0.0| Allows to auto-apply the coupons after adding delivery line


